//src//mylib.js

module.exports = {
    sum: (a, b) => a + b,
    random: () => {
        return Math.random();
    },
    arrayGen: () => [1, 2, 3],
    fibonacci: (n) => {
        var result;
        var fibArray = [0, 1];
        for (var i = 2; i < n; i++) {
            result = fibArray[i - 1] + fibArray[i - 2];
            fibArray.push(result);
        }

        return fibArray;
    },
    binarySearch: (value, array)=> {
        var first = 0;    //left endpoint
        var last = array.length - 1;   //right endpoint
        var position = -1;
        var found = false;
        var half;
     
        while (found === false && first <= last) {
            half = Math.floor((first + last)/2);
            if (array[half] == value) {
                found = true;
                position = half;
            } else if (array[half] > value) {  //if in lower half
                last = middle - 1;
            } else {  //in in upper half
                first = half + 1;
            }
        }
        return position;
    }
}