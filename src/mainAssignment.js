const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./myLibAssignment.js');

console.log({ 
    fibonacci : mylib.fibonacci(10)

});

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.get('/fib', (req, res) => {
    const n = parseInt(req.query.n);
    var array = mylib.fibonacci(n);
    res.send(`Result of Fibonacci: ${array}`);
});

app.get('/binary', (req, res) => {
    const a = parseInt(req.query.a);
    var array = [2, 5 , 9 , 10, 23, 34, 68];
    var element = 68;
    var num = mylib.binarySearch(element, array);
    res.send(`Number ${element} found in position ${num}`);
});

app.listen(port, () => {
    console.log(`Server : http://localhost:${port}`)
})