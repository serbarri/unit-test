// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js',() => {

    let myvar = undefined;
    var array = [2, 5 , 9 , 10, 23, 34, 68];

    it('myvar should exist', () => {
        should.exist(myvar);
    });

    before(() => {
        myvar = 1; //setup before testing
        console.log("Before testing...");
        fibArray = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34];
        
        console.log("First 10 numbers of Fibonacci sequence: " + fibArray);
        console.log("Array to test Binary Search algorithm: " + array);

    })
    it('Fibonacci succesion testing for the first 10 numbers', () => {
        const test = mylib.fibonacci(10);
        
        for(var i = 0; i < fibArray.length; i++){
            expect(test[i]).to.equal(fibArray[i]);
        }
       
    })

    it('Binary search testing using assert', () => {
        
        var element = 68;

        const index = mylib.binarySearch(element, array);   
        assert(index < array.length && index >= 0);
        assert(array[index] == element);
       
    })

    it('Should return 2 when sum function wiht a=1, b=1', ()=>{
        const result = mylib.sum(1,1); //1 +1
        expect(result).to.equal(2); // result expected to be equal 2
       
    });

    it('Parametrized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar); // 1 +1
        expect(result).to.equal(myvar + myvar);
    })

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') //true
    });

    after(() => {
        console.log("After testing...");
         var sumFib = 0;
         for(var i = 0; i < fibArray.length; i++){
            sumFib += fibArray[i];
         }
         console.log("Sum of the first 10 numbers of Fibonacci sequence: " + sumFib);
    })


})