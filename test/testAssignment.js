const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylibAssignment.js',() => {

    
    var array = [2, 5 , 9 , 10, 23, 34, 68];
    var element = 68;
    const test = mylib.fibonacci(10);
    const index = mylib.binarySearch(element, array);   

    before(() => {
        console.log("BEFORE TESTING");
        fibArray = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34];
        console.log("First 10 numbers of Fibonacci sequence: " + fibArray);
        console.log("Array to test Binary Search algorithm: " + array);
        console.log("Number we want to find: " + element);

    })
    it('Fibonacci function testing for the first 10 numbers', () => {
        
        for(var i = 0; i < fibArray.length; i++){
            expect(test[i]).to.equal(fibArray[i]);
        }
       
    })

    it('Binary search testing', () => {
    
        
        assert(index < array.length && index >= 0);
        assert(array[index] == element);
       
    })


    after(() => {
        console.log("AFTER TESTING");
         var sumFib = 0;
         for(var i = 0; i < fibArray.length; i++){
            sumFib += fibArray[i];
         }
         console.log("Number " + element + " found in position " + index);
         console.log("Array returned from fibonacci() function: " + test);
         console.log("Addition of the first 10 numbers of Fibonacci sequence: " + sumFib);
         
    })


})